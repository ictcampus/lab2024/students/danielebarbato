package com.ictcampus.lab.base.control.user;

import com.ictcampus.lab.base.control.model.UserResponse;
import com.ictcampus.lab.base.repo.entity.UserEntity;
import com.ictcampus.lab.base.service.user.UserService;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Controller for handling user-related operations.
 * @author kiko
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/v1/users")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/test", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<UserEntity> getUsers() {
        return userService.getUsers();
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserEntity getUserById(@PathVariable(name = "id") Long id) {
        return userService.getUserById(id);
    }

    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public void deleteUser(@PathVariable(name = "id") Long id) {
        userService.deleteUser(id);
    }
/**/
    @PutMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserEntity updateUser(
            @PathVariable(name = "id") Long id,
            @RequestBody UserEntity userRequest
    ) {
        log.info("Updating user with ID [{}] and data [{}]", id, userRequest);
        return userService.updateUser(id, userRequest.getUsername(), userRequest.getPassword());
    }

    @PostMapping(value = "/create", produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserEntity createUser(
            @RequestBody UserEntity userRequest
    ) {
        log.info("Creating a new user with data [{}]", userRequest);
        return userService.createUser(userRequest.getUsername(), userRequest.getPassword());
    }

    @PostMapping(value = "/findByUsername", produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserResponse findByUsername(
            @RequestBody String username
    ) {
        return userService.parseUserDetailsToUserResponse(userService.loadUserByUsername(username));
    }
}
