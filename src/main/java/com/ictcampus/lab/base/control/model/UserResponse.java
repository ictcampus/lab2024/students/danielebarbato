package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

/**
 * UserResponse is used to return user data to the client as JSON.
 *
 * @since 1.0.0
 */
@Value
@Builder
@Jacksonized
public class UserResponse {
    String username;
    List<String> authorities;
}
