package com.ictcampus.lab.base.service.user;
import com.ictcampus.lab.base.control.model.UserResponse;
import com.ictcampus.lab.base.repo.entity.UserEntity;
import com.ictcampus.lab.base.repo.user.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.GrantedAuthority;
import java.util.stream.Collectors;

import java.util.ArrayList;
import java.util.List;

/**
 * User logic implementation
 *
 * Provides CRUD operations for UserEntity and UserDetailsService implementation.
 * @author kiko
 * @since 1.0.0
 */
@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepo userRepo;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username) {
        UserEntity userEntity = userRepo.findByUsername(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>());
    }

    public List<UserEntity> getUsers() {
        return userRepo.findAll();
    }

    public UserEntity getUserById(long id) {
        return userRepo.findById(id);
    }

    public UserEntity createUser(String username, String password) {
        long id = userRepo.create(username, password);
        return userRepo.findById(id);
    }

    public UserEntity updateUser(long id, String username, String password) {
        userRepo.update(id, username, password);
        return userRepo.findById(id);
    }

    public void deleteUser(long id) {
        userRepo.delete(id);
    }
    public UserResponse parseUserDetailsToUserResponse(UserDetails userDetails) {
        return UserResponse.builder()
                .username(userDetails.getUsername())
                .authorities(userDetails.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList()))
                .build();
    }

}