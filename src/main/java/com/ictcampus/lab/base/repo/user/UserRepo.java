package com.ictcampus.lab.base.repo.user;

import com.ictcampus.lab.base.repo.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Types;
import java.util.Arrays;
import java.util.List;

/**
 * Repository for CRUD operations on UserEntity.
 * TODO Add Class Description
 * @author kiko
 * @since 1.0.0
 */
@AllArgsConstructor
@Repository
public class UserRepo {
    private final JdbcTemplate jdbcTemplate;

    public List<UserEntity> findAll() {
        return jdbcTemplate.query("SELECT * FROM users", new BeanPropertyRowMapper<>(UserEntity.class));
    }

    public UserEntity findById(long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE id = ?",
                new BeanPropertyRowMapper<>(UserEntity.class), id);
    }

    public UserEntity findByUsername(String username) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE username = ?",
                new BeanPropertyRowMapper<>(UserEntity.class), username);
    }

    @Transactional
    public long create(String username, String password) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        PreparedStatementCreatorFactory preparedStatementCreatorFactory = new PreparedStatementCreatorFactory(
                "INSERT INTO users (username, password) VALUES (?, ?)",
                Types.VARCHAR, Types.VARCHAR
        );
        preparedStatementCreatorFactory.setReturnGeneratedKeys(true);

        PreparedStatementCreator preparedStatementCreator = preparedStatementCreatorFactory
                .newPreparedStatementCreator(
                        Arrays.asList(username, password)
                );

        jdbcTemplate.update(preparedStatementCreator, generatedKeyHolder);

        return generatedKeyHolder.getKey().longValue();
    }

    public int update(long id, String username, String password) {
        return jdbcTemplate.update("UPDATE users SET username=?, password=? WHERE id=?", username, password, id);
    }

    public int delete(long id) {
        return jdbcTemplate.update("DELETE FROM users WHERE id=?", id);
    }
}