package com.ictcampus.lab.base.repo.entity;

import com.ictcampus.lab.base.repo.entity.AuthorEntity;
import com.ictcampus.lab.base.service.book.model.Author;
import jakarta.persistence.*;
import lombok.Data;

/**
 * TODO Add Class Description
 *
 * @AuthorEntity Daniele Barbato
 * @since 1.0.0
 */

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/*
 * BookEntity class describes the entity book in the logic of the software with all it's
 * attributes and informations as well with get and set for them.
 * */
@Data
@Entity
@Table(name = "books")
public class BookEntity {
    @Id
    private long id;
    private String title;
    @ManyToMany
    @JoinTable(
            name = "book_authors",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private List<AuthorEntity> authorEntity;
    private String description;
    private BigDecimal price;
    private float discount;
    private String isbn;
    private String descAbstract;
    private LocalDate publishLocalDate;
    private int sPosition;
    private String publisher;
    private int rPosition;
    @ElementCollection
    private List<String> urls;
    private String editor;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getDescAbstract() {
        return descAbstract;
    }

    public void setDescAbstract(String descAbstract) {
        this.descAbstract = descAbstract;
    }

    public LocalDate getPublishDate() {
        return publishLocalDate;
    }

    public void setPublishDate(LocalDate publishLocalDate) {
        this.publishLocalDate = publishLocalDate;
    }

    public String getPublisher() {
        return publisher;
    }



    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getrPosition() {
        return rPosition;
    }

    public void setrPosition(int rPosition) {
        this.rPosition = rPosition;
    }

    public int getsPosition() {
        return sPosition;
    }

    public void setsPosition(int sPosition) {
        this.sPosition = sPosition;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }


    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public List<AuthorEntity> getAuthors() {
        return authorEntity;
    }

    public void setAuthorEntity(List <AuthorEntity> authorEntity) {
        this.authorEntity = authorEntity;
    }

    public void setAuthors(List<Author> authors) {
    }
}

