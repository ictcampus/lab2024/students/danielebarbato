# Base Project

Il progetto è un esempio relativo allo sviluppo di un'applicazione web usando il 
framework SpringBoot. Ha un'architettura a tre livelli (control, service, data) e 
i dati di esempio vengono memorizzati in un DB H2. Le API RESTful esposte permettono
di eseguire le operazioni CRUD sull'entity World.

## Configurazioni del progetto
Le configurazione del progetto si trovano nel file
```bash
[root]/src/main/resources/application.yml
```

## URL per test
### Mostra tutti i pianeti presenti nel DB
* GET http://localhost:8080/api/v1/worlds

### Mostra un singolo pianeta presente nel DB, id identificata il pianeta
* GET http://localhost:8080/api/v1/worlds/{id}

### Crea un pianeta nel nel DB
* POST http://localhost:8080/api/v1/worlds

### Modifica un pianeta nel DB
* PUT http://localhost:8080/api/v1/worlds/{id}

### Elimina un pianeta dal DB
* DELETE http://localhost:8080/api/v1/worlds/{id}

## OpenApi
La documentazione online è disponibile all'indirizzo
http://localhost:8080/swagger-ui/index.html

## Compile
```bash
mvn -U clean install
```


## Run
```bash
mvn -U clean spring-boot:run
```
