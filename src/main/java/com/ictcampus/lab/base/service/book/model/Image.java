package com.ictcampus.lab.base.service.book.model;
import lombok.Data;

/**
 * Data to heandle the Image's value in the logic of the program
 * @author kiko
 * @since 1.0.0
 */
@Data
public class Image {
    private Long id;
    private String title;
    private String url;
    private boolean thumbnail;
}
