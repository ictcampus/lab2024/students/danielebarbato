package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import com.ictcampus.lab.base.service.book.model.Author;
import com.ictcampus.lab.base.service.book.model.Image;
import com.ictcampus.lab.base.service.book.model.Position;

/**
 * Book model to implement in the logic
 *
 * @author kiko
 */
@Data
public class Book {
    private Long id;
    private String title;
    private String isbn;
    private String descAbstract;
    private String description;
    private String publisher;
    private LocalDate publishedDate;
    private BigDecimal price;
    private BigDecimal discount;
    private List<Author> authors;
    private Image thumbnail;
    private List<Image> images;
    private Position position;
}