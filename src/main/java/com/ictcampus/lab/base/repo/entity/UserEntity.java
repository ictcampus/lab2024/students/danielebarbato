package com.ictcampus.lab.base.repo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * UserEntity is an object created to have instances of data relative to the user's data used to implement in the code
 *
 * @author Daniele Barbato
 */
@Data
@Table(name = "users")
@Entity
public class UserEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;
}
