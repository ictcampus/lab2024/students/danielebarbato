package com.ictcampus.lab.base.control.book.model;

import com.ictcampus.lab.base.service.book.model.Author;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;

/**
 * author response is used to return values to the client and have em displayed as a json
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Value
@Builder
@Jacksonized
public class AuthorResponse {
    Long id;
    String name;
    String surname;
    String nickname;
    LocalDate birthday;
    public Author toAuthor() {
        Author author = new Author();
        author.setId(this.id);
        author.setName(this.name);
        author.setSurname(this.surname);
        author.setNickname(this.nickname);
        author.setBirthday(this.birthday);
        return author;
    }
}
