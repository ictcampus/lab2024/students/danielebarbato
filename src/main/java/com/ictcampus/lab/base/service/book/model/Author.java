package com.ictcampus.lab.base.service.book.model;

import com.ictcampus.lab.base.control.book.model.AuthorResponse;
import com.ictcampus.lab.base.repo.entity.AuthorEntity;
import lombok.Data;

import java.time.LocalDate;

/**
 * Author is the model on which the logic is based
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
public class Author {
    private Long id;
    private String name;
    private String surname;
    private String nickname;
    private LocalDate birthday;
    public AuthorResponse toResponse() {
        return AuthorResponse.builder()
                .id(this.id)
                .name(this.name)
                .surname(this.surname)
                .nickname(this.nickname)
                .birthday(this.birthday)
                .build();
    }
    public static Author fromEntity(AuthorEntity authorEntity) {
        Author author = new Author();
        author.setId(authorEntity.getId());
        author.setName(authorEntity.getName());
        author.setSurname(authorEntity.getSurname());
        author.setNickname(authorEntity.getNickname());
        author.setBirthday(authorEntity.getBirthday());
        return author;
    }
    public AuthorEntity toEntity() {
        AuthorEntity authorEntity = new AuthorEntity();
        authorEntity.setId(this.id);
        authorEntity.setName(this.name);
        authorEntity.setSurname(this.surname);
        authorEntity.setNickname(this.nickname);
        authorEntity.setBirthday(this.birthday);
        return authorEntity;
    }
}
