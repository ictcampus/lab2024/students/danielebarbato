package com.ictcampus.lab.base.config.jwt;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * JwtRequestFilter is a filter class responsible for intercepting incoming requests
 * and validating JWT tokens for authentication.
 *
 * @author Daniele Barbato
 */

@Slf4j
@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal( HttpServletRequest request, HttpServletResponse response, FilterChain chain )
            throws ServletException, IOException {

        try {
            String jwt = extractJwt( request );
            if ( jwtUtil.validateJwtToken( jwt ) ) {
                String username = jwtUtil.extractUsername( jwt );

                if ( username != null && SecurityContextHolder.getContext().getAuthentication() == null ) {

                    UserDetails userDetails = this.userDetailsService.loadUserByUsername( username );

                    if ( jwtUtil.validateToken( jwt, userDetails ) ) {

                        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                                new UsernamePasswordAuthenticationToken( userDetails, null, userDetails.getAuthorities() );
                        usernamePasswordAuthenticationToken
                                .setDetails( new WebAuthenticationDetailsSource().buildDetails( request ) );
                        SecurityContextHolder.getContext().setAuthentication( usernamePasswordAuthenticationToken );
                    }
                }
            }
        } catch ( Exception e ) {
            log.error( "Cannot set user authentication: {}", e );
        }

        chain.doFilter( request, response );
    }

    private String extractJwt( HttpServletRequest request ) {
        final String authorizationHeader = request.getHeader( "Authorization" );

        if ( authorizationHeader != null && authorizationHeader.startsWith( "Bearer " ) ) {
            return authorizationHeader.substring( 7 );
        }

        return null;
    }
}
