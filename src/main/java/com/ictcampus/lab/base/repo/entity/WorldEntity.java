package com.ictcampus.lab.base.repo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/**
 * TODO Add Class Description
 *
 * @author Daniele Barbato
 * @since 1.0.0
 */

@Entity
public class WorldEntity {
    @Id
    private long id;
    private String name;
    private String system;

    public long getId() {
        return id;
    }

    public void setId( final long id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( final String name ) {
        this.name = name;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem( final String system ) {
        this.system = system;
    }
}
