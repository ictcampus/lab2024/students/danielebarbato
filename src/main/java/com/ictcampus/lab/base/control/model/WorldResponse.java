package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Daniele Barbato
 * @since 1.0.0
 */

public class WorldResponse {
	private Long id;
	private String name;
	private String system;

	public WorldResponse() {}
	public WorldResponse(Long id, String name, String system) {
		this.id	= id;
		this.name = name;
		this.system = system;
	}

	public Long getId() {
		return id;
	}

	public void setId( final Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( final String name ) {
		this.name = name;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem( final String system ) {
		this.system = system;
	}

	@Override
	public String toString() {
		return "WorldResponse [id=" + id + ", name=" + name + ", system=" + system + "]";
	}
}

