package com.ictcampus.lab.base.repo;

import com.ictcampus.lab.base.repo.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
    List<BookEntity> findByTitleContainingIgnoreCase(String title);
    BookEntity findByIsbn(String isbn);
}
