package com.ictcampus.lab.base.control.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * AuthorRequest is used to receive data from the client for author creation or update.
 * @author kiko  kiko
 * @since 1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorRequest {
    private Long id;
    private String name;
    private String surname;
    private String nickname;
    private LocalDate birthday;
}
