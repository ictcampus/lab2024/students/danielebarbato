    package com.ictcampus.lab.base.control.book.model;
    /**
     * TODO Add Class Description
     *
     * @author Daniele Barbato
     * @since 1.0.0
     */
    import lombok.AllArgsConstructor;
    import lombok.Data;
    import lombok.NoArgsConstructor;
    import com.ictcampus.lab.base.service.book.model.Author;

    import java.math.BigDecimal;
    import java.time.LocalDate;
    import java.util.List;

    /**
     * BookRequest is used to receive data from the client for book creation or update.
     *
     * @since 1.0.0
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class BookRequest {
        private String title;
        private String isbn;
        private String descAbstract;
        private String description;
        private String publisher;
        private LocalDate publishedDate;
        private BigDecimal price;
        private float discount;
        private List <Author> authors;

    }
