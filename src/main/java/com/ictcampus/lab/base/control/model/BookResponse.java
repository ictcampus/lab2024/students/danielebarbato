package com.ictcampus.lab.base.control.model;
import com.ictcampus.lab.base.service.book.model.Author;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import com.ictcampus.lab.base.service.book.model.Author;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import com.ictcampus.lab.base.control.book.model.AuthorResponse;

/**
 * BookResponse is used to return values to the client and have them displayed as JSON.
 *
 * @since 1.0.0
 */
@Value
@Builder
@Jacksonized
public class BookResponse {
	Long id;
	String title;
	String isbn;
	String descAbstract;
	String description;
	String publisher;
	LocalDate publishedDate;
	BigDecimal price;
	float discount;
	List<AuthorResponse> authors;

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getIsbn() {
		return isbn;
	}

	public String getDescAbstract() {
		return descAbstract;
	}

	public String getDescription() {
		return description;
	}

	public String getPublisher() {
		return publisher;
	}

	public LocalDate getPublishedDate() {
		return publishedDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public float getDiscount() {
		return discount;
	}

	public List<AuthorResponse> getAuthors() {
		return authors;
	}
}
