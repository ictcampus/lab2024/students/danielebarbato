package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Daniele Barbato
 * @since 1.0.0
 */

public class WorldRequest {
	private int id;
	private String name;
	private String system;

	public int getID() {return id;}
	public String getName() {
		return name;
	}

	public void setName( final String name ) {
		this.name = name;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem( final String system ) {
		this.system = system;
	}
	@Override
	public String toString() {
		return "WorldRequest [name=" + name + ", system=" + system + "]";
	}
}
