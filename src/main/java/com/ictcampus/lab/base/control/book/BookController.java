package com.ictcampus.lab.base.control.book;
import com.ictcampus.lab.base.service.book.model.Book;

import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.control.book.model.BookRequest;
import com.ictcampus.lab.base.service.book.BookService;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * TODO Add Class Description
 *
 * @author Daniele Barbato
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/v1/books")
@AllArgsConstructor
@Slf4j
public class BookController {

	private final BookService bookService;

	@GetMapping(value = "/test", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<BookResponse> getBooks() {
		return bookService.getBooks();
	}

	@DeleteMapping(value = "/del", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void deleteBook(@RequestBody long idToDel) {
		 bookService.deleteBook(idToDel);
		 log.info("Book with id {} was deleted", idToDel);
	}


	@PutMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void editBook(
			@PathVariable(name = "id") Long id,
			@RequestBody BookRequest bookRequest
	) {
		log.info("Aggiorno il libro con ID [{}] e dati [{}]", id, bookRequest);
		bookService.editBook(id, bookRequest);
	}

	@PostMapping(value = "/CB", produces = {MediaType.APPLICATION_JSON_VALUE})
	public long createBook(
			@RequestBody BookRequest bookRequest
	) {
		log.info("Creo un nuovo libro con i dati [{}]", bookRequest);
		return bookService.createBook(bookRequest);
	}

	@PostMapping(value = "/SearchOnDb", produces = {MediaType.APPLICATION_JSON_VALUE})
	public BookResponse SearchByNameDB(
			@RequestBody String name
	) {
		return bookService.searchByName(name);
	}
}
