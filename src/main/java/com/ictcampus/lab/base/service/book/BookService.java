package com.ictcampus.lab.base.service.book;
import com.ictcampus.lab.base.control.model.AuthorRequest;
import com.ictcampus.lab.base.repo.BookRepoImpl;
import com.ictcampus.lab.base.control.book.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.control.book.model.AuthorResponse;
import com.ictcampus.lab.base.repo.entity.BookEntity;
import com.ictcampus.lab.base.repo.entity.AuthorEntity;
import com.ictcampus.lab.base.service.book.model.Author;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO Add Class Description
 *
 * @since 1.0.0
 */
@Service
@AllArgsConstructor
public class BookService {

    private final BookRepoImpl bookRepoImpl;

    public List<BookResponse> getBooks() {
        return bookRepoImpl.findAll().stream()
                .map(this::convertBookEntityToResponse)
                .collect(Collectors.toList());
    }

    public void deleteBook(long idToDel) {
        bookRepoImpl.delete(idToDel);
    }

    public void editBook(Long id, BookRequest bookRequest) {
        BookEntity bookEntity = bookRepoImpl.findById(id);

        bookEntity.setTitle(bookRequest.getTitle());
        bookEntity.setDescription(bookRequest.getDescription());
        bookEntity.setPublisher(bookRequest.getPublisher());
        bookEntity.setPrice(bookRequest.getPrice());
        bookEntity.setDiscount(bookRequest.getDiscount());
        /*spaghetti code:*/
        List <AuthorEntity> aE = new ArrayList<>();
        List <Author> a =  bookRequest.getAuthors();
        for (Author ae : a) {
            aE.add(ae.toEntity());
        }
        bookEntity.setAuthorEntity(aE);
        /*end of spaghetti code:*/
        bookRepoImpl.create(bookEntity);
    }

    public long createBook(BookRequest bookRequest) {
        BookEntity bookEntity = new BookEntity();
        bookEntity.setTitle(bookRequest.getTitle());
        bookEntity.setIsbn(bookRequest.getIsbn());
        bookEntity.setDescAbstract(bookRequest.getDescAbstract());
        bookEntity.setDescription(bookRequest.getDescription());
        bookEntity.setPublisher(bookRequest.getPublisher());
        bookEntity.setPublishDate(bookRequest.getPublishedDate());
        bookEntity.setPrice(bookRequest.getPrice());
        bookEntity.setDiscount(bookRequest.getDiscount());
        bookEntity.setAuthors(bookRequest.getAuthors());
        convertBookEntityToResponse(bookEntity);
        return bookRepoImpl.create(bookEntity);
    }

    public BookResponse searchByName(String name) {
        BookEntity bookEntity = bookRepoImpl.findByName(name);
        return convertBookEntityToResponse(bookEntity);
    }

    private BookResponse convertBookEntityToResponse(BookEntity bookEntity) {
        return BookResponse.builder()
                .id(bookEntity.getId())
                .title(bookEntity.getTitle())
                .isbn(bookEntity.getIsbn())
                .descAbstract(bookEntity.getDescAbstract())
                .description(bookEntity.getDescription())
                .publisher(bookEntity.getPublisher())
                .publishedDate(bookEntity.getPublishDate())
                .price(bookEntity.getPrice())
                .discount(bookEntity.getDiscount())
                .authors(convertAuthorEntitiesToResponses(bookEntity.getAuthors()))
                .build();
    }

    private List<AuthorEntity> convertAuthorRequestsToEntities(List<AuthorRequest> authorRequests) {
        return authorRequests.stream()
                .map(request -> {
                    AuthorEntity entity = new AuthorEntity();
                    entity.setId(request.getId());
                    entity.setName(request.getName());
                    entity.setSurname(request.getSurname());
                    entity.setNickname(request.getNickname());
                    entity.setBirthday(request.getBirthday());
                    return entity;
                })
                .collect(Collectors.toList());
    }

    private List<AuthorResponse> convertAuthorEntitiesToResponses(List<AuthorEntity> authorEntities) {
        return authorEntities.stream()
                .map(entity -> AuthorResponse.builder()
                        .id(entity.getId())
                        .name(entity.getName())
                        .surname(entity.getSurname())
                        .nickname(entity.getNickname())
                        .build())
                .collect(Collectors.toList());
    }
}
