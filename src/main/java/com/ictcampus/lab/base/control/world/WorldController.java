package com.ictcampus.lab.base.control.world;

import com.ictcampus.lab.base.service.model.World;
import com.ictcampus.lab.base.control.exception.NotFoundException;
import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.service.model.WorldService;
import com.ictcampus.lab.base.repo.WorldRepo;
import com.ictcampus.lab.base.repo.entity.WorldEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/worlds")
@AllArgsConstructor
@Slf4j
public class WorldController {

	private final WorldService worldService;
	private final WorldRepo worldRepo;

	@GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<World> getWorlds() {
		log.info("Restituisco la lista dei pianeti [{}]", worldService.getWorlds());
		return worldService.getWorlds();
	}

	@GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public WorldResponse getWorld(@PathVariable(name = "id") Long id) throws NotFoundException {
		WorldResponse worldResponse = worldService.convertToWorldresponse(worldService.getWorldById(id));
		log.info("Restituisco il pianeta con ID [{}] e dati [{}]", id, worldResponse);
		return worldResponse;
	}

	@PostMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
	public Long createWorld(@RequestBody WorldRequest worldRequest) {
		log.info("Creo un nuovo pianeta con i dati [{}]", worldRequest);
		return worldService.addWorld(worldRequest.getName(), worldRequest.getSystem());
	}

	@PutMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void editWorld(@PathVariable(name = "id") Long id, @RequestBody WorldRequest worldRequest) {
		log.info("Aggiorno il pianeta con ID [{}] e dati [{}]", id, worldRequest);
		worldService.editWorld(id, worldRequest.getName(), worldRequest.getSystem());
	}

	@DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void deleteWorld(@PathVariable(name = "id") Long id) {
		log.info("Cancello il pianeta con ID [{}]", id);
		worldService.deleteWorld(id);
	}

	@GetMapping(value = "/findAll", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<WorldResponse> showWorldsDB() {
		List<WorldResponse> worlds = new ArrayList<>();
		List<WorldEntity> we = worldService.WorldListFromDB();
		for (WorldEntity w : we) {
			worlds.add(worldService.entityToWorldRes(w));
		}
		return worlds;
	}

	@PutMapping(value = "/update", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void updateWorld(@RequestBody WorldRequest worldRequest) {
		worldService.updateWorld(worldRequest.getID(), worldRequest.getName(), worldRequest.getSystem());
	}

	@PostMapping(value = "/createDB", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void createWorldDB(@RequestBody WorldRequest wr) {
		worldRepo.create(wr.getName(), wr.getSystem());
	}

	@GetMapping(value = "/search", produces = {MediaType.APPLICATION_JSON_VALUE})
	public WorldResponse searchWorldByName(@RequestParam String name) {
		return worldService.entityToWorldRes(worldRepo.findByName(name));
	}

	@GetMapping(value = "/searchDB", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<WorldResponse> searchWorldDB(@RequestParam(required = false) String name, @RequestParam(required = false) String system) {
		List<WorldResponse> worlds = new ArrayList<>();
		for (World we : worldService.getWorldsBySearch(name, system)) {
			worlds.add(worldService.convertToWorldresponse(we));
		}
		return worlds;
	}
}
